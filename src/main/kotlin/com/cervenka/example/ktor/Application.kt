package com.cervenka.example.ktor

import com.cervenka.example.ktor.persistence.CustomerRepository
import com.cervenka.example.ktor.plugins.configureCustomerSerialization
import com.cervenka.example.ktor.plugins.configureRouting
import com.cervenka.example.ktor.plugins.connectToPostgres
import io.ktor.server.application.*

fun main(args: Array<String>) {
    // Main entry point for the Ktor application
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    // Connect to the Postgres database
    connectToPostgres()

    // Create a repository instance
    val repository = CustomerRepository()

    // Configure routing and serialization
    configureRouting()
    configureCustomerSerialization(repository)
}
