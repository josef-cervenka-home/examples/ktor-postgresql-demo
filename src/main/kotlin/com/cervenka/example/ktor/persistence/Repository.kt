package com.cervenka.example.ktor.persistence

import com.cervenka.example.ktor.persistence.model.Customer

// Interface for the Customer repository
interface Repository {
    suspend fun getAll(): List<Customer>
    suspend fun create(customer: Customer): Customer
    suspend fun delete(id: Long): Boolean
}
