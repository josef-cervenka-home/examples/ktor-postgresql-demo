package com.cervenka.example.ktor.persistence.model

import kotlinx.serialization.Serializable

// Data class representing a Customer
@Serializable
data class Customer(val id: Long, val name: String)
