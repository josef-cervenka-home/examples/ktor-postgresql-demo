package com.cervenka.example.ktor.persistence

import com.cervenka.example.ktor.persistence.model.Customer
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

// Define the Customers table
object Customers : LongIdTable(name = "customer") {
    val name = varchar(name = "name", length = 50)
}

// Extension function to convert ResultRow to Customer
fun ResultRow.toCustomer() = Customer(
    id = this[Customers.id].value,
    name = this[Customers.name]
)

// Repository class for managing Customer data
class CustomerRepository : Repository {

    init {
        // Create the Customers table if it doesn't exist
        transaction {
            SchemaUtils.create(Customers)
        }
    }

    // Get all customers
    override suspend fun getAll(): List<Customer> = newSuspendedTransaction(Dispatchers.IO) {
        Customers.selectAll().map { it.toCustomer() }
    }

    // Create a new customer
    override suspend fun create(customer: Customer) : Customer = newSuspendedTransaction(Dispatchers.IO) {
        val id = Customers.insert {
            it[name] = customer.name
        }[Customers.id]
        Customer(id.value, customer.name)
    }

    // Delete a customer by ID
    override suspend fun delete(id: Long): Boolean = newSuspendedTransaction(Dispatchers.IO) {
        val rowsAffected = Customers.deleteWhere { Customers.id eq id }
        rowsAffected > 0
    }
}
