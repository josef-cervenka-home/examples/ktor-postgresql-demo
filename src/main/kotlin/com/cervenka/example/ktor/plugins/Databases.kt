package com.cervenka.example.ktor.plugins

import io.ktor.server.application.*
import org.jetbrains.exposed.sql.*
import java.sql.*

fun Application.connectToPostgres(): Connection {
    // Load the PostgreSQL driver
    Class.forName("org.postgresql.Driver")

    println("Trying to connect to Postgres DB")
    // Get environment variables
    val driver = environment.config.property("storage.driver").getString()
    val url = environment.config.property("storage.url").getString()
    val user = environment.config.property("storage.user").getString()
    val password = environment.config.property("storage.password").getString()
    println("Postgres url: $url")
    // Connect to the PostgreSQL database
    Database.connect(url, driver, user, password)
    return DriverManager.getConnection(url, user, password)

}
