package com.cervenka.example.ktor.plugins

import com.cervenka.example.ktor.persistence.Repository
import com.cervenka.example.ktor.persistence.model.Customer
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureCustomerSerialization(repository: Repository) {

    // Install ContentNegotiation plugin with JSON support
    install(ContentNegotiation) {
        json()
    }
    // Define routing for customer endpoints
    routing {
        route("/customer") {
            // Get all customers
            get {
                val customers = repository.getAll()
                call.respond(customers)
            }

            // Create a new customer
            post {
                val customer = call.receive<Customer>()
                val createdCustomer = repository.create(customer)
                call.respond(HttpStatusCode.Created, createdCustomer)
            }

            // Delete a customer by ID
            delete("/{id}") {
                val id = call.parameters["id"]?.toLongOrNull()
                if (id == null) {
                    call.respond(HttpStatusCode.BadRequest, "Invalid ID")
                    return@delete
                }
                val deleted = repository.delete(id)
                if (deleted) {
                    call.respond(HttpStatusCode.NoContent)
                } else {
                    call.respond(HttpStatusCode.NotFound)
                }
            }
        }
    }
}
