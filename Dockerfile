#FROM openjdk:11.0.16
#EXPOSE 8080:8080
#RUN mkdir /app
#COPY ./build/libs/*-all.jar /app/ktor-docker-sample.jar
#ENTRYPOINT ["java","-jar","/app/ktor-docker-sample.jar"]
#


FROM eclipse-temurin:17-jre-alpine

ADD build/libs/ktor-postgresql-demo-all.jar ktor-postgresql-demo-all.jar

EXPOSE 8080:8080

# Wait script - Wait for Postgres to be ready.
RUN apk add --no-cache bash
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/wait-for-it.sh

CMD ["sh", "-c", "java -jar ktor-postgresql-demo-all.jar"]
