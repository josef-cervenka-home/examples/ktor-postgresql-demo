import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val ktorVersion: String by project
val kotlinVersion: String by project
val logbackVersion: String by project

plugins {
    application
    war
    kotlin("jvm") version "1.9.23"
    id("io.ktor.plugin") version "2.3.12"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.23"
}

group = "com.cervenka.example.ktor"
version = "0.0.1"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

repositories {
    mavenCentral()
//    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/ktor/eap") }
//    maven { url = uri("https://plugins.gradle.org/m2/") } // Add this line
}

dependencies {
    implementation("io.ktor:ktor-server-core-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-host-common-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-status-pages-jvm:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktorVersion")
    implementation("org.postgresql:postgresql:42.7.3")
//    implementation("com.h2database:h2:2.1.214")
    implementation("org.jetbrains.exposed:exposed-core:0.41.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.41.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.41.1")
    implementation("io.ktor:ktor-server-netty-jvm:$ktorVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("io.ktor:ktor-server-config-yaml:2.3.10")
    testImplementation("io.ktor:ktor-server-tests-jvm:$ktorVersion")
    testImplementation("io.ktor:ktor-client-content-negotiation-jvm:$ktorVersion")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "17"
    }
}

task<Exec>("buildWithoutTests") {
    if (System.getProperty("os.name").lowercase().contains("win"))
        commandLine("gradlew.bat", "build", "-x", "test")
    else
        commandLine("./gradlew", "build", "-x", "test")
}

task<Exec>("buildDockerImage") {
    dependsOn("buildWithoutTests")
    commandLine("docker", "build", "--force-rm", "-t", "ktor_example_image", ".")
}

task<Exec>("runDockerCompose") {
    dependsOn("buildDockerImage")
    commandLine("docker", "compose", "up", "-d")
}