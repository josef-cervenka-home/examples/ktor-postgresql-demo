# Docker Compose

A sample Ktor project showing how to run a Ktor application consisting of a PostgreSQL database and backend

### PostgreSQL connection
You may want to edit those .env files:
- ./env/postgres-variables.env
- ./env/server-variables.env

If you decide to change POSTGRES_DB, don't forget to also change database in docker-compose.yml healthcheck

## Running

To run this application under Docker Compose, execute command bellow:

   ```Bash
   ./gradlew runDockerCompose
   ```